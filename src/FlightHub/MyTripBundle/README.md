mytrip
======

A Symfony project implements Rest API described in user story [here](/src/FlightHub/MyTripBundle/Docs/user_story_mytrip_rest_api.pdf)


3rd party bundles used in this project:
---------------------------------------

* FOSRestBundle
* JMSSerializerBundle
* NelmioApiDocBundle

Local custom bundle that implements the mytrip Rest API:
--------------------------------------------------------

MyTripBundle located in src/FlightHub/MyTripBundle


System requirements
===================

 PHP >= 5.5.9
 Web server
 MySQL server
 http://symfony.com/doc/current/reference/requirements.html

Installation
============

Step 1: Install composer
------------------------

Follow the instructions on https://getcomposer.org/doc/00-intro.md

Browse to mytrip application directory and run - composer update

Step 2: Configure web server
----------------------------

Follow the instruction on http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html

Step 3: Populate with fixture data (optional)
---------------------------------------------

Browse to application folder and run commands:

php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load

Demo, Documentation and Sandbox
==============================

Step 1: Edit you local machine host file
----------------------------------------

Open the host file on your local machine and add to the end of the file: 46.101.112.224  mytrip.ca

Step 2: Open your browser and open mytrip.ca
--------------------------------------------

The API methods and their description will be shown as a list.

To use a Sandbox, click on a title of a specific API method and click on a Sandbox link (near the Documentation link)


Runing Tests
============

Step 1: Install PHPUnit
-----------------------

Follow the instruction on https://phpunit.de/manual/current/en/installation.html

Step 2: Run PHPUnit
-------------------

Browse to bundle folder in your local environment and run command: phpunit
