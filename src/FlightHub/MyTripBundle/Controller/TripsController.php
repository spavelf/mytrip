<?php

namespace FlightHub\MyTripBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

use FOS\RestBundle\Controller\Annotations\View;

use FlightHub\MyTripBundle\Entity\Flight;
use FlightHub\MyTripBundle\Entity\Trip;
use FlightHub\MyTripBundle\Form\Type\TripType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * Class TripsController
 * @package FlightHub\MyTripBundle\Controller
 */
class TripsController extends FOSRestController
{
    /**
     * @View(serializerGroups={"Trip","Flight","Airport"})
     * @ApiDoc(
     *  description="Retrieve list of trips",
     *  resource=true,
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function cgetAction()
    {
        $data = $this->getDoctrine()
          ->getRepository('FlightHubMyTripBundle:Trip')->findAll();
        return $data;
    }

    /**
     * @View(serializerGroups={"Trip","Flight","Airport"})
     * @param $slug
     * @return FlightHubMyTripBundle:Trip
     * @ParamConverter("trip", class="FlightHubMyTripBundle:Trip",options={"id" = "slug"})
     * @ApiDoc(
     *  description="Retrieve trip",
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="slug",
     *          "dataType"="guid",
     *          "requirement"="existing trip id",
     *          "description"="trip id"
     *      }
     *  },
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function getAction(Trip $trip)
    {
        return $trip;
    }

    /**
     * @return Response
     * @ParamConverter("editedTrip", converter="fos_rest.request_body", options={"deserializationContext"={"groups"={"Trip"}}})
     * @ApiDoc(
     *  description="Edit trip",
     *  parameters={
     *      {"name"="id", "dataType"="guid", "required"=true, "description"="trip id"},
     *      {"name"="name", "dataType"="string", "required"=false, "description"="trip name"}
     *  },
     *  statusCodes = {
     *    204 = "Returned when successfully updated",
     *  }
     *
     * )
     */
    public function editAction(Trip $editedTrip)
    {
        $trip = $this->getDoctrine()
          ->getRepository('FlightHubMyTripBundle:Trip')
          ->findOneBy(
            array('id' => $editedTrip->getId())
          );

        if(!$trip){
            throw new ResourceNotFoundException("Resource Not Found");
        }

        //Update the trip with the edited properties values
        $serializer = $this->container->get('serializer');
        foreach($serializer->toArray($editedTrip) as $key => $value) {
            if($key!="id" && isset($value)) {
                $setter = "set" . ucfirst($key);
                $trip->$setter($value);
            }
        }

        return $this->processRequest($trip);
    }

    /**
     * @param $slug
     * @return ArrayCollection of FlightHubMyTripBundle:Flight
     * @View(serializerGroups={"Airport", "Flight"})
     * @ApiDoc(
     *  description="Retrieve list of all flights from the trip ordered by departure by ascending order",
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="slug",
     *          "dataType"="guid",
     *          "requirement"="existing trip id",
     *          "description"="trip id"
     *      }
     *  },
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function getFlightsAction($slug)
    {
        $trip = $this->getDoctrine()
            ->getRepository('FlightHubMyTripBundle:Trip')
            ->findOneBy(
                array('id' => $slug)
            );

        $flights = $trip->getFlights();
        return $flights;
    }

    /**
     * @param $slug
     * @param $id
     * @return Response
     * @ApiDoc(
     *  description="Remove flight from the trip",
     *  requirements={
     *      {
     *          "name"="slug",
     *          "dataType"="guid",
     *          "requirement"="existing trip id",
     *          "description"="trip id"
     *      },
     *      {
     *          "name"="id",
     *          "dataType"="guid",
     *          "requirement"="existing flight id",
     *          "description"="flight id"
     *      }
     *  },
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function unlinkFlightAction($slug, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $trip = $em->getRepository('FlightHubMyTripBundle:Trip')->findOneBy(
                array('id' => $slug)
            );

        $flight = $em->getRepository('FlightHubMyTripBundle:Flight')->findOneBy(
                array('id' => $id)
            );

        $trip->removeFlight($flight);
        $em->flush();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param $slug
     * @param $id
     * @return Response
     * @ApiDoc(
     *  description="Add flight to the trip",
     *  requirements={
     *      {
     *          "name"="slug",
     *          "dataType"="guid",
     *          "requirement"="existing trip id",
     *          "description"="trip id"
     *      },
     *      {
     *          "name"="id",
     *          "dataType"="guid",
     *          "requirement"="existing flight id",
     *          "description"="flight id"
     *      }
     *  },
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function linkFlightAction($slug, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $trip = $em->getRepository('FlightHubMyTripBundle:Trip')->findOneBy(
          array('id' => $slug)
        );

        $flight = $em->getRepository('FlightHubMyTripBundle:Flight')->findOneBy(
          array('id' => $id)
        );

        $trip->addFlight($flight);
        $em->flush();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Trip $trip
     * @return Response
     */
    private function processRequest(Trip $trip)
    {
        $response = new Response();
        $statusCode = $trip->getId() ? 204 : 201;

        $validator = $this->get('validator');
        $errors = $validator->validate($trip);

        if (!count($errors)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();

            // set the `Location` header only when creating new resources
            if (201 === $statusCode) {
                $response->headers->set('Location',
                  $this->generateUrl(
                    'flighthub_mytrip_get_trip', array('slug' => $trip->getId())
                  )
                );
            }
        }
        else {
            $statusCode = 400;
        }

        $response->setStatusCode($statusCode);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function getErrorMessages($form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[][] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }


}
