<?php

namespace FlightHub\MyTripBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AirportsController
 * @package FlightHub\MyTripBundle\Controller
 */
class AirportsController extends Controller
{
    /**
     * @View(serializerGroups={"Airport","City"})
     * @ApiDoc(
     *  description="Retrieve list of airports (alphabetically) ",
     *  resource=true,
     *  statusCodes = {
     *    200 = "Returned when successful",
     *  }
     * )
     */
    public function cgetAction()
    {
        $data = $this->getDoctrine()
            ->getRepository('FlightHubMyTripBundle:Airport')
            ->findBy(
                array(),
                array('name' => 'ASC')
            );

        return $data;
    }
}
