<?php

namespace FlightHub\MyTripBundle\Tests;

use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader as DataFixturesLoader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\ORM\Tools\SchemaTool;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

class WebTestCase extends BaseWebTestCase
{
    protected $em;

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();

        $kernel = static::createKernel();
        $kernel->boot();
        $bundle = $kernel->getBundle("FlightHubMyTripBundle");
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();
        $loader = new DataFixturesLoader($container);

        // Recreate schema and the tables
        static::recreateDB($em);

        // Load all fixtures for bundle
        static::loadFixtures($em, $loader, $bundle);
    }

    public static function tearDownAfterClass() {

        parent::setUpBeforeClass();

        $kernel = static::createKernel();
        $kernel->boot();
        $bundle = $kernel->getBundle("FlightHubMyTripBundle");
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();
        $loader = new DataFixturesLoader($container);

        // Recreate schema and the tables
        static::recreateDB($em);

        // Load all fixtures for bundle
        static::loadFixtures($em, $loader, $bundle);
    }

    public function setUp(){
        $this->client = static::createClient();
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
          ->get('doctrine')
          ->getManager();
    }

    protected function assertJsonResponse($response, $statusCode = 200) {
        $this->assertEquals(
          $statusCode,
          $response->getStatusCode(),
          $response->getContent()
        );
        if($statusCode!=204) {
        $this->assertTrue(
          $response->headers->contains('Content-Type', 'application/json'),
          $response->headers
        );
        }
    }

    private static function loadFixtures($em, $loader, $bundle)
    {
        $path = $bundle->getPath().'/DataFixtures/ORM';

        if (is_dir($path)) {
            $loader->loadFromDirectory($path);
        }

        $fixtures = $loader->getFixtures();

        if (!$fixtures) {
            throw new InvalidArgumentException('Could not find any fixtures to load in');
        }
        $purger = new ORMPurger($em);
        $executor = new ORMExecutor($em, $purger);
        $executor->execute($fixtures, true);
    }

    private static function recreateDB($em)
    {
        $schemaTool = new SchemaTool($em);
        $metadata = $em->getMetadataFactory()->getAllMetadata();

        // Drop and recreate tables for all entities
        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);
    }
}
