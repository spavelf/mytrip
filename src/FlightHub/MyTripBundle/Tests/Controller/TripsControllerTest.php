<?php

namespace FlightHub\MyTripBundle\Tests;

use FlightHub\MyTripBundle\Tests\WebTestCase;

class TripsControllerTest extends WebTestCase
{
    public function testGetFlights()
    {
        $trips = $this->em->getRepository('FlightHubMyTripBundle:Trip')->findAll();
        $trip = $trips[0];

        $flights = $trip->getFlights();

        $this->client->request('GET', '/trips/' . $trip->getId() . '/flights');
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response);

        $jsonResponseCollection = json_decode($response->getContent());

        for($i=0; $i < count($flights); $i++) {
            $this->assertEquals($flights[$i]->getId(), $jsonResponseCollection[$i]->id);
        }
    }

    public function testLinkFlight ()
    {
        $countFlightBeforeLink = 0;
        $countFlightAfterLink = 0;

        $trips = $this->em->getRepository('FlightHubMyTripBundle:Trip')->findAll();
        $trip = $trips[0];

        //Remove the first flight in the flights collection and
        //Ensure the flight was removed
        $flights = $trip->getFlights();
        $flight = $flights[0];
        $id = $flight->getId();

        $trip->removeFlight($flight);
        $this->em->flush();
        $this->client->request('GET', '/trips/' . $trip->getId() . '/flights');
        $response = $this->client->getResponse();
        $jsonResponseCollection = json_decode($response->getContent());
        for($i=0; $i < count($jsonResponseCollection); $i++) {
            if($id == $jsonResponseCollection[$i]->id) {
                $this->fail("Failure in the test setup - Flight was not removed from the trip");
            }
        }

        //Add earlier removed flight from the trip by calling the tested Rest API method and
        //Ensure the flight was added to the trip
        $this->client->request('LINK', '/trips/' . $trip->getId() . '/flights/' . $id);
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response);
        $this->client->request('GET', '/trips/' . $trip->getId() . '/flights');
        $response = $this->client->getResponse();
        $jsonResponseCollection = json_decode($response->getContent());
        for($i=0; $i < count($jsonResponseCollection); $i++) {
            if($id == $jsonResponseCollection[$i]->id) {
                $countFlightAfterLink++;
            }
        }
        $this->assertEquals($countFlightAfterLink, $countFlightBeforeLink +1);
    }

    public function testUnlinkFlight ()
    {
        $trips = $this->em->getRepository('FlightHubMyTripBundle:Trip')->findAll();
        $trip = $trips[0];

        //Remove the first flight in the flights collection by calling appropriate Rest API method
        $flights = $trip->getFlights();
        $flight = $flights[0];
        $id = $flight->getId();

        $this->client->request('UNLINK', '/trips/' . $trip->getId() . '/flights/' . $id);
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response);

        //Ensure the flight was removed
        $this->client->request('GET', '/trips/' . $trip->getId() . '/flights');
        $response = $this->client->getResponse();
        $jsonResponseCollection = json_decode($response->getContent());
        for($i=0; $i < count($jsonResponseCollection); $i++) {
            if($id == $jsonResponseCollection[$i]->id) {
                $this->fail("Failure in the test setup - Flight was not removed from the trip");
            }
        }
    }

    public function testEdit ()
    {
        $trips = $this->em->getRepository('FlightHubMyTripBundle:Trip')->findAll();
        $trip = $trips[0];

        $name = $trip->getName();
        $newName = 'New Trip';
        $this->assertNotEquals($name, $newName);

        $this->client->request(
            'POST',
            '/trips',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"id":"' . $trip->getId() . '","name":"' . $newName . '"}'
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response, 204);

        $this->client->request('GET', '/trips/' . $trip->getId());
        $response = $this->client->getResponse();
        $jsonResponseCollection = json_decode($response->getContent());
        if(count($jsonResponseCollection) == 1) {
            $jsonResponseCollection = array($jsonResponseCollection);
        }
        for($i=0; $i < count($jsonResponseCollection); $i++) {
            if($trip->getId() == $jsonResponseCollection[$i]->id) {
                $this->assertEquals($jsonResponseCollection[$i]->name, $newName);
            }
        }
    }

    public function testGet()
    {
        $trips = $this->em->getRepository('FlightHubMyTripBundle:Trip')->findAll();

        $trip = $trips[0];

        $this->client->request('GET', '/trips/' . $trip->getId());
        $response = $this->client->getResponse();
        $jsonResponseCollection = json_decode($response->getContent());

        $this->assertJsonResponse($response);
        $this->assertEquals($trip->getId(), $jsonResponseCollection->id);
    }
}
