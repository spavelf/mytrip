<?php

namespace FlightHub\MyTripBundle\Tests;

use FlightHub\MyTripBundle\Tests\WebTestCase;

class AirportsControllerTest extends WebTestCase
{
    public function testCget()
    {
        $dbCollection = $this->em->getRepository('FlightHubMyTripBundle:Airport')
          ->findBy(
            array(),
            array('name' => 'ASC')
          );

        $this->client->request('GET', '/airports');
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response);

        $jsonResponseCollection = json_decode($response->getContent());

        for($i=0; $i < count($dbCollection); $i++) {
            $this->assertEquals($dbCollection[$i]->getId(), $jsonResponseCollection[$i]->id);
        }
    }
}
