<?php

namespace FlightHub\MyTripBundle\Entity;

use JMS\Serializer\Annotation\Groups;

/**
 * City
 */
class City
{
    /**
     * @var guid
     * @Groups({"City"})
     */
    private $id;

    /**
     * @var string
     * @Groups({"City"})
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $airports;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->airports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return guid
     *
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add airport
     *
     * @param \FlightHub\MyTripBundle\Entity\Airport $airport
     *
     * @return City
     */
    public function addAirport(\FlightHub\MyTripBundle\Entity\Airport $airport)
    {
        $this->airports[] = $airport;

        return $this;
    }

    /**
     * Remove airport
     *
     * @param \FlightHub\MyTripBundle\Entity\Airport $airport
     */
    public function removeAirport(\FlightHub\MyTripBundle\Entity\Airport $airport)
    {
        $this->airports->removeElement($airport);
    }

    /**
     * Get airports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAirports()
    {
        return $this->airports;
    }
}
