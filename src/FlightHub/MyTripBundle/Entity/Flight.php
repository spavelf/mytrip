<?php

namespace FlightHub\MyTripBundle\Entity;

use JMS\Serializer\Annotation\Groups;

/**
 * Flight
 */
class Flight
{
    /**
     * @var guid
     * @Groups({"Flight"})
     */
    private $id;

    /**
     * @var \DateTime
     * @Groups({"Flight"})
     */
    private $departure;

    /**
     * @var \DateTime
     * @Groups({"Flight"})
     */
    private $arrival;

    /**
     * @var \FlightHub\MyTripBundle\Entity\Airport
     * @Groups({"Flight"})
     */
    private $fromAirport;

    /**
     * @var \FlightHub\MyTripBundle\Entity\Airport
     * @Groups({"Flight"})
     */
    private $toAirport;

    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set departure
     *
     * @param \DateTime $departure
     *
     * @return Flight
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return \DateTime
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param \DateTime $arrival
     *
     * @return Flight
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return \DateTime
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set fromAirport
     *
     * @param \FlightHub\MyTripBundle\Entity\Airport $fromAirport
     *
     * @return Flight
     */
    public function setFromAirport(\FlightHub\MyTripBundle\Entity\Airport $fromAirport)
    {
        $this->fromAirport = $fromAirport;

        return $this;
    }

    /**
     * Get fromAirport
     *
     * @return \FlightHub\MyTripBundle\Entity\Airport
     */
    public function getFromAirport()
    {
        return $this->fromAirport;
    }

    /**
     * Set toAirport
     *
     * @param \FlightHub\MyTripBundle\Entity\Airport $toAirport
     *
     * @return Flight
     */
    public function setToAirport(\FlightHub\MyTripBundle\Entity\Airport $toAirport)
    {
        $this->toAirport = $toAirport;

        return $this;
    }

    /**
     * Get toAirport
     *
     * @return \FlightHub\MyTripBundle\Entity\Airport
     */
    public function getToAirport()
    {
        return $this->toAirport;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $trips;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trips = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trip
     *
     * @param \FlightHub\MyTripBundle\Entity\Trip $trip
     *
     * @return Flight
     */
    public function addTrip(\FlightHub\MyTripBundle\Entity\Trip $trip)
    {
        $this->trips[] = $trip;
        return $this;
    }

    /**
     * Remove trip
     *
     * @param \FlightHub\MyTripBundle\Entity\Trip $trip
     */
    public function removeTrip(\FlightHub\MyTripBundle\Entity\Trip $trip)
    {
        $this->trips->removeElement($trip);
    }

    /**
     * Get trips
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrips()
    {
        return $this->trips;
    }
}
