<?php

namespace FlightHub\MyTripBundle\Entity;

use JMS\Serializer\Annotation\Groups;

/**
 * Airport
 */
class Airport
{
    /**
     * @var guid
     * @Groups({"Airport"})
     *
     */
    private $id;

    /**
     * @Groups({"Airport"})
     * @var string
     */
    private $name;

    /**
     * @var string
     * @Groups({"Airport"})
     */
    private $code;

    /**
     * @var \FlightHub\MyTripBundle\Entity\City
     * @Groups({"City"})
     */
    private $city;


    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Airport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Airport
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set city
     *
     * @param \FlightHub\MyTripBundle\Entity\City $city
     *
     * @return Airport
     */
    public function setCity(\FlightHub\MyTripBundle\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \FlightHub\MyTripBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
}
