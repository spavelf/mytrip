<?php

namespace FlightHub\MyTripBundle\Entity;

use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

/**
 * Trip
 */
class Trip
{
    /**
     * @var guid
     * @Groups({"Trip"})
     * @Type("string")
     */
    private $id;

    /**
     * @var string
     * @Groups({"Trip"})
     * @Type("string")
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Groups({"Trip"})
     */
    private $flights;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flights = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Trip
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add flight
     *
     * @param \FlightHub\MyTripBundle\Entity\Flight $flight
     *
     * @return Trip
     */
    public function addFlight(\FlightHub\MyTripBundle\Entity\Flight $flight)
    {
        $this->flights[] = $flight;
        return $this;
    }

    /**
     * Remove flight
     *
     * @param \FlightHub\MyTripBundle\Entity\Flight $flight
     */
    public function removeFlight(\FlightHub\MyTripBundle\Entity\Flight $flight)
    {
        $this->flights->removeElement($flight);
    }

    /**
     * Get flights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFlights()
    {
        return $this->flights;
    }
}

