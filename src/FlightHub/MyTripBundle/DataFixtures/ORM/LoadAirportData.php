<?php
// LoadAirportData.php

namespace FlightHub\MyTripBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FlightHub\MyTripBundle\Entity\Airport;


class LoadAirportData extends AbstractFixture implements OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {

        $montrealCity = $this->getReference('montreal-city');

        //Montreal airports
        $yulAirport = new Airport();
        $yulAirport->setName('Montreal / Pierre Elliott Trudeau International Airport');
        $yulAirport->setCode('YUL');
        $yulAirport->setCity($montrealCity);
        $manager->persist($yulAirport);

        $yhuAirport = new Airport();
        $yhuAirport->setName('Montréal / Saint-Hubert Airport');
        $yhuAirport->setCode('YHU');
        $yhuAirport->setCity($montrealCity);
        $manager->persist($yhuAirport);

        $berlinCity = $this->getReference('berlin-city');

        //Berlin airports
        $sxfAirport = new Airport();
        $sxfAirport->setName('Berlin-Schönefeld International Airport');
        $sxfAirport->setCode('SXF');
        $sxfAirport->setCity($berlinCity);
        $manager->persist($sxfAirport);

        $txlAirport = new Airport();
        $txlAirport->setName('Berlin-Tegel International Airport');
        $txlAirport->setCode('TXL');
        $txlAirport->setCity($berlinCity);
        $manager->persist($txlAirport);

        $munichCity = $this->getReference('munich-city');

        //Munich airport
        $mucAirport = new Airport();
        $mucAirport->setName('Munich International Airport');
        $mucAirport->setCode('MUC');
        $mucAirport->setCity($munichCity);
        $manager->persist($mucAirport);


        $manager->flush();

        $this->addReference('montreal-yul-airport', $yulAirport);
        $this->addReference('montreal-yhu-airport', $yhuAirport);
        $this->addReference('berlin-sxf-airport', $sxfAirport);
        $this->addReference('berlin-txl-airport', $txlAirport);
        $this->addReference('munich-muc-airport', $mucAirport);
    }

    public function getOrder()
    {
        return 2;
    }

}