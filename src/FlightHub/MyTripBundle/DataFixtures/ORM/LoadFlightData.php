<?php
// LoadAirportData.php

namespace FlightHub\MyTripBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FlightHub\MyTripBundle\Entity\Flight;
use Symfony\Component\Finder\Tests\Iterator\DateRangeFilterIteratorTest;


class LoadFlightData extends AbstractFixture implements OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {
        $referenceDateTime = new \DateTimeImmutable("2016-06-15T00:00:00+00:00");

        $firstFlight = new Flight();
        $firstFlight->setFromAirport($this->getReference('montreal-yul-airport'));
        $firstFlight->setDeparture($referenceDateTime->add(new \DateInterval('PT15H30M')));
        $firstFlight->setToAirport($this->getReference('berlin-txl-airport'));
        $firstFlight->setArrival($firstFlight->getDeparture()->add(new \DateInterval('PT7H59M')));
        $manager->persist($firstFlight);

        $secondFlight = new Flight();
        $secondFlight->setFromAirport($this->getReference('berlin-txl-airport'));
        $secondFlight->setDeparture($firstFlight->getArrival()->add(new \DateInterval('PT2H')));
        $secondFlight->setToAirport($this->getReference('munich-muc-airport'));
        $secondFlight->setArrival($secondFlight->getDeparture()->add(new \DateInterval('PT1H7M')));
        $manager->persist($secondFlight);

        $thirdFlight = new Flight();

        $thirdFlight->setFromAirport($this->getReference('munich-muc-airport'));
        $thirdFlight->setDeparture($referenceDateTime->add(new \DateInterval('P7DT4H45M')));
        $thirdFlight->setToAirport($this->getReference('montreal-yul-airport'));
        $thirdFlight->setArrival($thirdFlight->getDeparture()->add(new \DateInterval('PT8H9M')));
        $manager->persist($thirdFlight);

        $manager->flush();

        $this->addReference('first-flight', $firstFlight);
        $this->addReference('second-flight', $secondFlight);
        $this->addReference('third-flight', $thirdFlight);
    }

    public function getOrder()
    {
        return 3;
    }

}