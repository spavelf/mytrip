<?php
// LoadCityData.php

namespace FlightHub\MyTripBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FlightHub\MyTripBundle\Entity\City;


class LoadCity extends AbstractFixture implements OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {
        $montrealCity = new City();
        $montrealCity->setName('Montreal');
        $manager->persist($montrealCity);

        $berlinCity = new City();
        $berlinCity->setName('Berlin');
        $manager->persist($berlinCity);

        $munichCity = new City();
        $munichCity->setName('Munich');
        $manager->persist($munichCity);

        $manager->flush();

        $this->addReference('montreal-city', $montrealCity);
        $this->addReference('berlin-city', $berlinCity);
        $this->addReference('munich-city', $munichCity);
    }

    public function getOrder()
    {
        return 1;
    }

}