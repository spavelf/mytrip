<?php
// LoadAirportData.php

namespace FlightHub\MyTripBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FlightHub\MyTripBundle\Entity\Trip;


class LoadTripData extends AbstractFixture implements OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {
        $myTrip = new Trip();

        $myTrip->setName('My trip');

        $myTrip->addFlight($this->getReference('first-flight'));
        $myTrip->addFlight($this->getReference('second-flight'));
        $myTrip->addFlight($this->getReference('third-flight'));
        $manager->persist($myTrip);

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }

}