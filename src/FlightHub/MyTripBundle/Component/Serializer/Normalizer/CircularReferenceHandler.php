<?php

namespace FlightHub\MyTripBundle\Component\Serializer\Normalizer;

class CircularReferenceHandler {
    public static function setCircularReferenceHandler($object) {
        return $object->getId();
    }
} 