<?php
// TripType.php

namespace FlightHub\MyTripBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('id')
          ->add('name')
        ;
    }

    public function getName()
    {
        return 'trip';
    }
}

